PROJ_NAME = webapp-kube
REPO_URL = https://github.com/darkzera/$(PROJ_NAME).git  
CLONE_DIR = ./$(PROJ_NAME) 
TARGET_DIR = ./$(PROJ_NAME)/$(target)
TAG = 1

clone: 
	git clone $(REPO_URL) $(CLONE_DIR);

build:
	cd $(CLONE_DIR) && mvn clean && mvn package -DSskipTests

test:
	cd $(CLONE_DIR) && mvn test 

publish:
	cd $(CLONE_DIR)/target && docker build -t hello:v7 . && docker image tag hello:v7 localhost:5001/hello7 && docker push localhost:5001/hello7 


deploy: 
	kubectl apply -f hello-kub-deployment.yaml
	
