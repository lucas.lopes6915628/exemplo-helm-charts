### Helm exemplo  

#### Dependencias

- Kubernetes 
- Docker
- Java app
- Kind (ou minikube)
- Helm 

/kubernetes-manual-deployment -> Arquivos de inicializacao manual dos pods

Webapp -> aplicacao que vamos fazer o deploy.  Simples que responde um Hello world + IP no endpoint

Makefile -> Simula nossa pipeline

Helm -> pasta do projeto Helm. A partir dela que vamos gerar as <del>imagens do helm charts</del> 

L2Advt.yaml e kind-localregistry.sh -> Faz o redirecionamento da rede para que as requisicoes cheguem com sucesso aos pods internos do Kub
