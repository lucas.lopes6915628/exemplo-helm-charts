package com.klearning.dockerimg.Learning.docker.and.kube;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

@SpringBootApplication
public class LearningDockerAndKubeApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearningDockerAndKubeApplication.class, args);
	}

}
